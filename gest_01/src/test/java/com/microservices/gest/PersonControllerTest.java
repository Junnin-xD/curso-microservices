package com.microservices.gest;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import com.microservices.gest.controller.PersonController;
import com.microservices.gest.models.PersonModel;
import com.microservices.gest.services.PersonService;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonControllerTest {
	@Autowired
	private PersonController personController;
	@MockBean
	private PersonService personService;
	
	@Test
	public void findAllTest() {
		List<PersonModel> personModel = new ArrayList();
		
		Mockito.when(personService.findAll()).thenReturn(personModel);
		
		List<PersonModel> personResponse = personController.findAll();
		
		Assertions.assertEquals(personResponse, personModel);
	}
}
