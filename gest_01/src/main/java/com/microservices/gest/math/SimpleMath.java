package com.microservices.gest.math;

public class SimpleMath {

	public Double som(Double firstNumber, Double secondNumber) {
		Double som = firstNumber + secondNumber;
		return som;
	}
	public Double sub(Double firstNumber, Double secondNumber) {
		Double sub = firstNumber - secondNumber;
		return sub;
	}
	public Double mul(Double firstNumber, Double secondNumber) {
		Double mul = firstNumber * secondNumber;
		return mul;
	}
	public Double div(Double firstNumber, Double secondNumber) {
		Double div = firstNumber / secondNumber;
		return div;
	}
	public Double med(Double firstNumber, Double secondNumber) {
		Double med = (firstNumber + secondNumber) / 2;
		return med;
	}
	public Double raiz(Double number) {
		Double raiz = (Double) Math.sqrt(number);
		return raiz;
	}
}
