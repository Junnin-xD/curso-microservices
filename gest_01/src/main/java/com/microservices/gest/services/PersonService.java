package com.microservices.gest.services;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Service;

import com.microservices.gest.models.PersonModel;

@Service
public class PersonService {

	private final AtomicLong counter = new AtomicLong();
	
	public PersonModel create(PersonModel person) {
		return person;
	}
	
	public PersonModel update(PersonModel person) {
		return person;
	}
	
	public void delete(String id) {
		
	}
	
	public PersonModel findById(int i) {
		PersonModel personModel = new PersonModel();
		personModel.setId(counter.incrementAndGet());
		personModel.setFirstName("Alexsandro");
		personModel.setLastName("junior");
		personModel.setAddress("testando");
		personModel.setGender("M");
		return personModel;
	}

	public List<PersonModel> findAll(){
		List<PersonModel> person = new ArrayList<PersonModel>();
		for (int i = 0; i < 8; i++) {
			PersonModel Persons = mockPersonModel(i);
			person.add(Persons);
		}
		return person;
	}

	private PersonModel mockPersonModel(int i) {
		PersonModel personModel = new PersonModel();
		personModel.setId(counter.incrementAndGet());
		personModel.setFirstName("Alexsandro" + i);
		personModel.setLastName("junior");
		personModel.setAddress("testando");
		personModel.setGender("M");
		return personModel;
	}
	
}
